'use strict';

var xhr = new XMLHttpRequest();
setInterval(() => {
  xhr.open(
    "GET",
    "https://neto-api.herokuapp.com/comet/pooling",
    false
  );

  xhr.send();
  xhr.addEventListener("load", getNumber('.pooling', xhr.responseText));
}, 1000);

function getNumber(ContainerClass, number) {
  if (typeof(+number.trim()) !== 'number') {
    return;
  }
  const container = document.querySelector(ContainerClass);
  if (container.querySelector('.flip-it')) {
    container.querySelector('.flip-it').classList.remove('flip-it');
  }
  container.children[number.trim()].classList.add('flip-it');
}