'use strict';

var xhr = new XMLHttpRequest();
setInterval(() => {

  xhr.open(
    "GET",
    "https://neto-api.herokuapp.com/comet/long-pooling",
    false
  );

  xhr.send();
  
  xhr.addEventListener("load", getNumber('.long-pooling', xhr.responseText));
}, 10000);