'use strict';

let connection = new WebSocket('wss://neto-api.herokuapp.com/draw');

connection.addEventListener('open', () => {
  console.log('Вебсокет-соединение открыто');
  editor.addEventListener('update', sendImage);
});

function sendImage(event) {
  event.canvas.toBlob(function(blob){
    connection.send(blob);
  })
}